# Install latest version of krud globally
npm i -g .
if [[ $? -ne 0 ]] ; then
    exit 1
fi

# Clean up the testing directory
rm -rf ./test/services
rm -rf ./test/routers
mkdir ./test/services
mkdir ./test/routers

# Run krud in the test dir
cd ./test
krud models/test.json

# Run the server
node server
