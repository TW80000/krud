#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const MODEL_FILEPATH = process.argv[2];
const SERVICE_FILEPATH = process.argv[3];
const ROUTER_FILEPATH = process.argv[4];
const CONNECTION_STRING = process.argv[5];

checkUsage();

const MODEL_NAME = MODEL_FILEPATH.split('.json')[0].split('/').reverse()[0];
const COLLECTION_NAME = MODEL_NAME.toLowerCase() + 's';
const RELATIVE_SERVICE_FILEPATH = path.posix.relative(path.posix.dirname(ROUTER_FILEPATH), SERVICE_FILEPATH)

genUserFile('service-template.js', SERVICE_FILEPATH);
genUserFile('router-template.js', ROUTER_FILEPATH);

console.log(`
   ▄█   ▄█▄    ▄████████ ███    █▄  ████████▄  
  ███ ▄███▀   ███    ███ ███    ███ ███   ▀███ 
  ███▐██▀     ███    ███ ███    ███ ███    ███ 
 ▄█████▀     ▄███▄▄▄▄██▀ ███    ███ ███    ███ 
▀▀█████▄    ▀▀███▀▀▀▀▀   ███    ███ ███    ███ 
  ███▐██▄   ▀███████████ ███    ███ ███    ███ 
  ███ ▀███▄   ███    ███ ███    ███ ███   ▄███ 
  ███   ▀█▀   ███    ███ ████████▀  ████████▀  
  ▀           ███    ███ `);

console.log(`
Success! The router and service files were both generated correctly. All that
needs to be done now is to add the router to your Express server file:

    app.use('/api/${COLLECTION_NAME}', require('${ROUTER_FILEPATH}'));

Once you've done that, you'll have the following REST API available:

    GET    /api/${COLLECTION_NAME}      => Get all ${COLLECTION_NAME}
    GET    /api/${COLLECTION_NAME}/{id} => Get a specific ${MODEL_NAME}
    POST   /api/${COLLECTION_NAME}      => Create a new ${MODEL_NAME}
    DELETE /api/${COLLECTION_NAME}/{id} => Delete a specific ${MODEL_NAME}
    PUT    /api/${COLLECTION_NAME}/{id} => Replace a specific ${MODEL_NAME}
    PATCH  /api/${COLLECTION_NAME}/{id} => Partially update a specific ${MODEL_NAME}`);


// ----------------------------------------------------------------------------


/** Makes sure that the command was called correctly. */
function checkUsage() {
  if (path.posix.extname(MODEL_FILEPATH) !== '.json' || process.argv.length !== 6) {
    console.error("Usage: krud MODEL.json SERVICE.js ROUTER.js DB_CONNECTION_STRING");
    process.exit(-1);
  }
}

/** Check if a given filepath ends with a given extension */
function hasExtension(filepath, extension) {
  const rgx = new RegExp("^.*?\." + extension);
  return rgx.test(filepath);
}

/**
 * Generates a user file given a template file and a model name
 * @param {String} templateFilename The filename of the template
 * @param {String} dest The destination filepath
 */
function genUserFile(templateFilename, dest) {

  // Format stuff
  const templateFilepath = path.posix.join(__dirname, templateFilename);

  // Check if the target file already exists
  if (fs.existsSync(dest)) {
    console.warn(`File '${dest}' already exists, skipping...`);
    return;
  }

  // Read the template file
  const templateFile = fs.readFileSync(templateFilepath, { encoding: 'utf-8' });

  // Replace the placeholders with the actual stuff
  const userServiceFile = templateFile
    .replace(/__entity__/g, MODEL_NAME)
    .replace(/__collectionName__/g, COLLECTION_NAME)
    .replace(/__connectionString__/g, CONNECTION_STRING)
    .replace(/__serviceFilepath__/g, RELATIVE_SERVICE_FILEPATH)
    .replace(/__entityFile__/g, MODEL_FILEPATH);

  // Save the new file
  fs.writeFileSync(dest, userServiceFile, 'utf-8');
}
