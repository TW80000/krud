# Tests the API
# Run build.sh first to make sure the server builds and is running

echo "GET /api/tests"
curl -s http://127.0.0.1:8080/api/tests
echo -e "\n"

echo "POST /api/tests"
curl -s -H "Content-Type: application/json" -X POST -d '{"name": "Joe Smith", "age": 42}' http://127.0.0.1:8080/api/tests
