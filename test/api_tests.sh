echo "################################################################################"
echo "########################   RUNNING API TESTS   #################################"
echo "################################################################################"

base_url='http://127.0.0.1:8080/api/tests'

echo ""
echo "GET $base_url"
curl -s $base_url | python -m json.tool

echo ""
echo ""
echo "GET $base_url/abc123"
curl -s $base_url/abc123 | python -m json.tool

echo ""
echo ""
echo "POST $base_url"
curl -s -X POST -H "Content-Type: application/json" -d '{"id": "1n4l1k", "name": "Dennis", "age": 101, "rating": 8.34, "registered": false}' $base_url | python -m json.tool

echo ""
echo "GET $base_url/1n4l1k"
curl -s $base_url/1n4l1k | python -m json.tool

echo ""
echo ""
echo "DELETE $base_url/abc123"
curl -s -X DELETE $base_url/abc123 | python -m json.tool

echo ""
echo ""
echo "GET $base_url?age=101"
curl -s "$base_url?age=101" | python -m json.tool

echo ""
echo ""
echo "[Failure] GET $base_url?name=Alice"
curl -s "$base_url?name=Alice" | python -m json.tool

echo ""
echo ""
echo "PUT $base_url/1n4l1k"
curl -s -X PUT -H "Content-Type: application/json" -d '{"id": "1n4l1k", "name": "Dennis", "age": 102, "rating": 8.34, "registered": false}' "$base_url/1n4l1k" | python -m json.tool

echo ""
echo ""
echo "[Failure] GET $base_url?age=101"
curl -s "$base_url?age=101" | python -m json.tool

echo ""
echo ""
echo "GET $base_url?age=102"
curl -s "$base_url?age=102" | python -m json.tool

echo ""
echo ""
echo "PATCH $base_url/1n4l1k"
curl -s -X PATCH -H "Content-Type: application/json" -d '{"age": 103}' "$base_url/1n4l1k" | python -m json.tool

echo ""
echo ""
echo "[Failure] GET $base_url?age=102"
curl -s "$base_url?age=102" | python -m json.tool

echo ""
echo ""
echo "GET $base_url?age=103"
curl -s "$base_url?age=103" | python -m json.tool

echo ""
