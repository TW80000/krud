/**
 * A simple express server to test if the generated files work.
 */

const express = require('express');
const bodyParser = require('body-parser');
const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.use('/api/tests', require('./routers/testRouter.js'));

server.listen(8080, () => console.log('Listening on port 8080.'));
