echo "Removing routers and services"
rm routers/*
rm services/*

echo ""
echo "################################################################################"
echo "##########################   RUNNING TESTS   ###################################"
echo "################################################################################"

echo ""
echo "[Failure] Passing a model file without .json extension"
krud server.js services/testService.js routers/testRouter.js mongodb://localhost:27017/test

echo ""
echo "Using krud to generate service and router"
krud models/test.json services/testService.js routers/testRouter.js mongodb://localhost:27017/test

echo ""
echo "Running server"
node server &
sleep 5 # FIXME

echo ""
echo "Populating Database with test data"
node populate_database.js

echo ""
echo "################################################################################"
echo "########################   RUNNING UNIT TESTS   ################################"
echo "################################################################################"

echo ""
echo "Running service tests"
node service_tests.js

echo ""
echo "Populating Database with test data"
node populate_database.js

echo ""
./api_tests.sh

echo ""
echo "Killing server"
node_pid=$(ps | grep node | awk '{ print $1 }')
kill $node_pid
