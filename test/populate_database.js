const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
const dbName = 'test';

(async () => {

  let client;

  try {

    client = await MongoClient.connect(url);
    const db = client.db(dbName);

    console.log("Removing all documents");
    await db.collection('tests').remove({});

    console.log("Inserting sample documents");
    await db.collection('tests').insertMany([
      {
        name: "Alice",
        age: 49,
        id: "abc123",
        rating: 1.23,
        registered: true
      },
      {
        name: "Bob",
        age: 52,
        id: "asd109",
        rating: 2.29,
        registered: true
      },
      {
        name: "Charlie",
        age: 14,
        id: "lk31jl",
        rating: 4.96,
        registered: false
      },
    ]);

  } catch (err) {
    console.log(err.stack);
  }

  client.close();

})();
