const service = require('./services/testService.js');
const assert = require('assert');

// Initialize the generated service
service.init('mongodb://127.0.0.1:27017')
    .then(runTests)
    .catch(err => console.log(err.actual))
    .then(() => { process.exit(0); });

function runTests() {

    // Test that validate does not throw any errors for a valid test object
    assert.doesNotThrow(() => { service.validate({
        id: "abc123",
        name: "Alice",
        age: 49,
        rating: 1.43,
        registered: true
    }); }, TypeError);

    // Test that validate throws a TypeError when a string field is not a string
    assert.throws(() => { service.validate({
        id: 123456,
        name: "Alice",
        age: 49,
        rating: 1.43,
        registered: true
    }); }, TypeError);

    // Test that validate throws a TypeError when an integer field is not an
    // integer
    assert.throws(() => { service.validate({
        id: "abc123",
        name: "Alice",
        age: "49",
        rating: 1.43,
        registered: true
    }); }, TypeError);

    // Test that validate throws a TypeError when a float field is not a float
    assert.throws(() => { service.validate({
        id: "abc123",
        name: "Alice",
        age: 49,
        rating: "1.43",
        registered: true
    }); }, TypeError);

    // Test that validate throws a TypeError when a boolean field is not a
    // boolean
    assert.throws(() => { service.validate({
        id: "abc123",
        name: "Alice",
        age: 49,
        rating: 1.43,
        registered: "true"
    }); }, TypeError);

}
