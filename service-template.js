const fs = require('fs');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const __entity__File = fs.readFileSync('__entityFile__', { encoding: 'utf-8' });
const __entity__ = JSON.parse(__entity__File);

/**
 * Provides common methods for working with __entity__'s.
 */
class __entity__Service {
  constructor() {
    this.db = null;
    this.dbConnectionErr =
      '__entity__Service was not initialized before use. You must call the `init()` method before using other methods.';
  }

  /**
   * Initialize the class by opening a database connection.
   * @param {String} connectionString The connection string to connect to the database
   */
  async init(connectionString) {
    try {
      this.db = await MongoClient.connect(connectionString);
    } catch (err) {
      console.error(
        "Error: Couldn't connect to the database.\n\n\tDid you start it with `mongod`?\n\tDid you provide the right connection URI?\n"
      );
      console.log(err);
      process.exit(-1);
    }
  }

  /**
   * Checks if the given argument is a valid __entity__.
   * @param {*} obj The object to check.
   * @returns {void} Returns nothing if validation was successful.
   * @throws {TypeError} If the object was not a valid __entity__.
   */
  validate(obj) {
    for (let prop in obj) {
      if (!__entity__.hasOwnProperty(prop)) {
        throw new TypeError(`Given object has property "${prop}" but entity schema does not`);
      }
    }
    for (let prop in __entity__) {
      if (__entity__.hasOwnProperty(prop)) {
        switch (__entity__[prop]) {
          case "integer":
            if (!Number.isSafeInteger(obj[prop])) {
              throw new TypeError(`Property "${prop}" should have been a safe integer.`);
            }
            break;
          case "float":
            if (typeof obj[prop] !== 'number' || Number.isInteger(obj[prop]) || Number.isNaN(obj[prop])) {
              throw new TypeError(`Property "${prop}" should have been a safe float.`);
            }
            break;
          default:
            if (typeof obj[prop] !== __entity__[prop]) {
              throw new TypeError(`Property "${prop}" was of type '${typeof obj[prop]}' but it should have been of type '${__entity__[prop]}'.`);
            }
            break;
        }
      }
    }
  }

  /**
   * Check if the given string is the name of a property on all __entity__s
   * @param {String} prop Any string
   * @returns {Boolean} True if `prop` is a valid property of any __entity__
   */
  hasProperty(prop) {
    return __entity__.hasOwnProperty(prop);
  }

  /**
   * Get the type of a given property on all __entity__s
   * @param {String} prop The property name to check
   * @throws {Error} When the provided string is not a property of all __entity__s
   * @returns {String} A string representing the type of the given property
   */
  typeOfProperty(prop) {

    if (!this.hasProperty(prop)) {
      const PropError = new Error(`Property "${prop}" is not a property of __entity__s`);
      PropError.code = 900; // Arbitrary
      throw PropError;
    }

    return __entity__[prop];
  }

  /**
   * Gets a particular __entity__ from the database.
   * @param {String} id The id of the __entity__.
   * @throws {Error} If no __entity__ with the given ID was found.
   * @returns {__entity__} A __entity__ with id equal to the given id.
   */
  async getById(id) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    const collection = this.db.collection('__collectionName__');
    const entity = await collection.findOne({ id: id });

    if (entity === null) {
      const NoEntitiesError = new Error(`No __entity__ with id ${id} found.`);
      NoEntitiesError.code = 404;
      throw NoEntitiesError;
    }

    return entity;
  }

  /**
   * Gets all __entity__s from the database that match the query parameters.
   * If no query parameters are provided, return all __entity__s.
   * @returns {Array<__entity__>} An array of all __entity__s matching the
   * given query parameters.
   */
  async getAll(queryParams = {}) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    const collection = this.db.collection('__collectionName__');
    const __entity__s = await collection.find(queryParams).toArray();

    if (__entity__s === null) {
      const NoEntitiesError = new Error(`No __entity__s found.`);
      NoEntitiesError.code = 500;
      throw NoEntitiesError;
    }

    return __entity__s;
  }

  /**
   * Creates a new __entity__ and saves it to the database
   * @param {__entity__} obj The __entity__ to create
   * @throws {TypeError} If the object was not a valid __entity__
   * @throws {MongoError} If there was an error inserting the __entity__
   */
  async create(obj) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    this.validate(obj);

    const collection = this.db.collection('__collectionName__');
    await collection.insertOne(obj);
  }

  /**
   * Updates the given fields of a particular __entity__ with the object provided
   * @param {String} id The id of the __entity__ to update
   * @param {Object} obj The fields and new values to update on the __entity__
   * @throws {Error} If no __entity__ with the given ID was found
   * @returns {void} If the update was successful
   */
  async update(id, obj) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    await this.getById(id);

    const collection = this.db.collection('__collectionName__');
    await collection.updateOne({id: id}, {'$set': obj});
  }

  /**
   * Replaces a particular __entity__ with the __entity__ provided
   * @param {String} id The id of the __entity__ to update
   * @param {__entity__} obj The __entity__ to replace the existing __entity__ with
   * @throws {TypeError} If the object was not a valid __entity__
   * @throws {Error} If no __entity__ with the given ID was found
   * @returns {void} If the update was successful
   */
  async replace(id, obj) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    this.validate(obj);

    await this.getById(id);

    const collection = this.db.collection('__collectionName__');
    await collection.replaceOne({id: id}, obj);
  }

  /**
   * Deletes a particular __entity__.
   * @param {String} id The ID of the __entity__ to delete.
   * @returns {void} If the deletion was successful.
   * @throws {Error} If no __entity__ with the given ID was found.
   */
  async delete(id) {

    if (this.db === null) {
      const DbError = new Error(this.dbConnectionErr);
      DbError.code = 500;
      throw DbError;
    }

    await this.getById(id);

    const collection = this.db.collection('__collectionName__');
    await collection.removeOne({id: id});
  }
}

module.exports = new __entity__Service();
