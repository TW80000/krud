# krud

krud is an opinionated boilerplate code generator. krud is called opinionated because it generates code specifically for [Express](expressjs.com) and [MongoDB](https://www.mongodb.com/). Pull Requests adding support for other databases and servers are welcome, but supporting other databases and backend libraries is not a goal of krud without sponsorship.

You give krud a JSON file that represents some entity you care about and krud generates a REST API for you to operate on that entity.

## Example Use Case

Let's say you want to create a blog, and you want to store each blog post as an object in your db. You might imagine one of those objects looking something like this:

```json
{
    "id": "19v1k4",
    "title": "My Trip to Toronto",
    "published": true,
    "date_created": 1513000774667,
    "content": "I went to Toronto to visit family ..."
}
```

To represent such a record in an entity to be used by krud, we create a file named BlogPost.json that contains the following:

```json
{
    "id": "string",
    "title": "string",
    "published": "boolean",
    "date_created": "integer",
    "content": "string"
}
```

Now that we have our entity, we can use krud. On the command line, run:

```shell
krud entities/BlogPost.json services/BlogPostService.js routers/BlogPostRouter.js mongodb://127.0.0.1:27017/mydb
```

After running the above, you should see the following output:

```

   ▄█   ▄█▄    ▄████████ ███    █▄  ████████▄
  ███ ▄███▀   ███    ███ ███    ███ ███   ▀███
  ███▐██▀     ███    ███ ███    ███ ███    ███
 ▄█████▀     ▄███▄▄▄▄██▀ ███    ███ ███    ███
▀▀█████▄    ▀▀███▀▀▀▀▀   ███    ███ ███    ███
  ███▐██▄   ▀███████████ ███    ███ ███    ███
  ███ ▀███▄   ███    ███ ███    ███ ███   ▄███
  ███   ▀█▀   ███    ███ ████████▀  ████████▀
  ▀           ███    ███

Success! The router and service files were both generated correctly. All that
needs to be done now is to add the router to your Express server file:

    app.use('/api/blogposts', require('routers/BlogPostRouter.js'));

Once you've done that, you'll have the following REST API available:

    GET    /api/blogposts      => Get all blogposts
    GET    /api/blogposts/{id} => Get a specific BlogPost
    POST   /api/blogposts      => Create a new BlogPost
    DELETE /api/blogposts/{id} => Delete a specific BlogPost
    PUT    /api/blogposts/{id} => Replace a specific BlogPost
    PATCH  /api/blogposts/{id} => Partially update a specific BlogPost
```

And that's it!

## Installing

To install krud globally, run:

```shell
npm i -g krud
```

## Usage

```shell
krud ENTITY.json SERVICE.js ROUTER.js DB_CONNECTION_STRING
```

This will read your entity structure (similar to a schema) from the file given as the first argument and use that information to generate the service and router files.

### Format of entity JSON file

This is the complete list of supported types:

* string
* boolean
* integer
* float

If you would like to have fields that are objects or arrays, try serializing them into a string. Support for objects and arrays may be added at a later time.

The only other constraint is that you must have an `id` field in your object that is of type "string" if you want the generated code to work out of the box. Of course, you don't have to do this for your application, but if you don't include it in your entity you will have to change the generated code to work with whatever identification scheme you use.

## Philosophy

Design decisions and trade-offs are made to adhere to the following philosopy:

krud is meant to be run **once** to generate generic code. You are supposed to have to edit and/or delete some of the code that krud generates. krud is **purposefully designed not to be configurable**. You cannot tell krud what you want it to generate. It will **assume** what you want and you can edit what it generates to suit your needs.

This philosophy has the following benefits:

  * It doesn't constrain the dev who's using it. You don't have to work within the constraints that krud has built in. krud just gives you code that you can change into anything you want.
  * It is far easier to write highly opinionated code than highly configurable code, and therefore krud will actually get shipped instead of forever being a pipe dream that can do everything I could ever ask for. Once krud gets shipped, I'm open to making it more configurable.

## Opinions, Assumptions, Constraints, and Omissions

krud assumes:

* You're using express
* You're using MongoDB
* You have the idea of an "entity" that is important to your application like a Tweet, a TodoItem, a Profile, or literally any other "thing" that you can describe as a JSON object
* You want to be able to create, read, update, and delete these entities in your db
* You want a REST API to do these operations
* You also want a JavaScript class with methods to do these operations
* You want your entities to be validated against your "schema" before being used in the operations

krud does **not** take into account authentication. Adding code to handle authentication was considered but ultimately decided against for the following reasons:

* Not every project needs authentication
* There are too many assumptions to make around authentication. Some include transmission protocol (HTTP Basic Auth, tokens, cookies, etc.), libraries (passport.js or others), sessions (how to store the session tokens? are they even using tokens? which library?)

If you will need authentication for your project, you can still use krud, but you'll need to implement the authentication features yourself.

## Dependencies

* node >= 8
* express
* MongoDB

## Development

To try out your local copy of krud, run

```shell
npm i -g .
```

in the project root. Then you can use `krud` from the command line. A test harness script has been added in the `test` folder. If you're on Windows, use git bash to run it.

```shell
cd test
chmod +x ./run_tests.sh
./run_tests.sh
```

## License

MIT
