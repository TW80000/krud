const __entity__Service = require('__serviceFilepath__');
__entity__Service.init('__connectionString__');
const router = require('express').Router();

/**
 * GET /
 * Return an array of all __entity__s
 * If query parameters are supplied, use them to filter by fields on the entity
 */
router.get('/', async (req, res) => {
  try {

    // Keep only the query params where the key matches the name of a field
    // on any __entity__
    const queryKeys = Object.keys(req.query).filter(query => __entity__Service.hasProperty(query));
    const queryParams = {};
    try {
      queryKeys.map(key => {
        const type = __entity__Service.typeOfProperty(key);
        if (type === 'integer') {
          queryParams[key] = parseInt(req.query[key]);
        } else if (type === 'string') {
          queryParams[key] = req.query[key];
        } else if (type === 'float') {
          queryParams[key] = parseFloat(req.query[key]);
        } else if (type === 'boolean') {
          queryParams[key] = req.query[key] === "true";
        }
      });
    } catch (err) {
      res.status(400).json({ message: "One of the query parameters couldn't be parsed correctly. Check the types", type: "error" });
    }
    const __entity__s = await __entity__Service.getAll(queryParams);
    return res.status(200).json(__entity__s);
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

/**
 * GET /{id}
 * Get an __entity__ by its id
 */
router.get('/:id', async (req, res) => {
  try {
    const __entity__ = await __entity__Service.getById(req.params.id);
    return res.status(200).json(__entity__);
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

/**
 * POST /
 * Create a new __entity__
 */
router.post('/', async (req, res) => {
  try {
    await __entity__Service.create(req.body);
    return res
      .status(201)
      .json({ message: 'Successfully created __entity__', type: 'success' });
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

/**
 * PUT /{id}
 * Replace a specific __entity__
 */
router.put('/:id', async (req, res) => {
  try {
    await __entity__Service.replace(req.params.id, req.body);
    return res
      .status(201)
      .json({ message: 'Successfully updated __entity__', type: 'success' });
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

/**
 * PATCH /{id}
 * Partially update a specific __entity__
 */
router.patch('/:id', async (req, res) => {
  try {
    await __entity__Service.update(req.params.id, req.body);
    return res
      .status(201)
      .json({ message: 'Successfully updated __entity__', type: 'success' });
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

/**
 * DELETE /{id}
 * Delete a specific __entity__
 */
router.delete('/:id', async (req, res) => {
  try {
    await __entity__Service.delete(req.params.id);
    return res
      .status(200)
      .json({ message: 'Successfully deleted __entity__', type: 'success' })
  } catch (err) {
    return res
      .status(err.code || 500)
      .json({ message: err.message, type: 'error' });
  }
});

module.exports = router;
